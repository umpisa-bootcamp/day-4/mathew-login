import React, {useState, useEffect} from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'
import { createBrowserHistory } from "history";
import UserRegister from './containers/Authentication/Register';
import UserLogin from './containers/Authentication/Login';
import HomePage from './containers/homepage/HomePage';
import CreateAppliance from './containers/Create/CreateAppliance';
function App() {
  const history = createBrowserHistory();
  return (
    <Router history={history}>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/register">Register</Link>
            </li>
            <li>
              <Link to="/login">Login</Link>
            </li>
            <li>
              <Link to="/">Homepage</Link>
            </li>
            <li>
              <Link to='/create'>Create Appliances</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path='/register' exact component={UserRegister}></Route>
          <Route path='/login' exact component={UserLogin}></Route>
          <Route path='/create' exact component={CreateAppliance}/>
          <Route path='/' exact component={HomePage}/>
        </Switch>
      </div>
    </Router>
  );
}

export default App
