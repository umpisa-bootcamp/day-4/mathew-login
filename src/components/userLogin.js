import React, {useState, useEffect} from 'react';
import {Redirect} from 'react-router-dom';

const UserLogin =({
    user,
    onCreate,
    // onChangeField
}) => {
    const [state, setState] = useState({
        email: '',
        password: '',

    });

    useEffect(() => {
        setState({
            ...state,
            email: user.email,
            password: user.password
        })
    }, [user])

    const onChangeField = (event, field) => {
        setState({
            ...state,
            [field]: event.target.value
        })
    }
    const onSubmit = () => {
        if(user){
            return <Redirect to="/home"/>
        }
    }
return(
    <div>
    <p>Email Address</p>
    <input
        type="text"
        onChange={e => onChangeField(e, 'email')}
        defaultValue={state.email} />
    <p>Password</p>
    <input
        type="password"
        onChange={e => onChangeField(e, 'password')}
        defaultValue={state.password} />
        <br />
        <p>
        <button onClick={() => onSubmit(state)}>LogIn</button>

        </p>
</div>
)
}
export default UserLogin
