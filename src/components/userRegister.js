import React, {useState, useEffect} from 'react';

const UserRegister =({
    user,
    onCreate,
    // onChangeField
}) => {
    const [state, setState] = useState({
        name: user ? user.name : '',
        email: user ? user.email:'',
        password: user ? user.password:'',

    });

    useEffect(() => {
        setState({
            ...state,
            name: user.name,
            email: user.email,
            password: user.password
        })
    }, [user])

    const onChangeField = (event, field) => {
        setState({
            ...state,
            [field]: event.target.value
        })
    }
    const onSubmit = (data) => {
        onCreate(data)
    }
return(
    <div>
    <p>Name</p>
    <input
        type="text"
        onChange={e => onChangeField(e, 'name')}
        defaultValue={state.name} />
    <p>Email Address</p>
    <input
        type="text"
        onChange={e => onChangeField(e, 'email')}
        defaultValue={state.email} />
    <p>Password</p>
    <input
        type="password"
        onChange={e => onChangeField(e, 'password')}
        defaultValue={state.password} />
        <br />
        <p>
        <button onClick={() => onSubmit(state)}>Register</button>

        </p>
</div>
)
}
export default UserRegister
