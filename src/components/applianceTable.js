import React from 'react'

const applianceTable = ({appliances,
   fetchAppliances,
    onClickRow, 
    deleteAppliance,
    onClickEdit
  }) => {

    return(
        <div>
            <button onClick={fetchAppliances}>Fetch</button>
  <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Specification</th>
          </tr>
        </thead>
        <tbody>
          {appliances.map((appliance, index) =>
          <tr 
          key={`tr_appliances_${index}`}
          onClick={() => onClickRow(appliance)}>
            <td>{appliance.name}</td>
            <td>{appliance.specification}</td>
            <td>
              <button onClick={() => onClickEdit(appliance)}>EDIT</button>
            </td>
            <td>
              <button onClick={() => deleteAppliance(appliance.id)}>DELETE</button>
            </td>
          </tr>)}
        </tbody>
      </table>
        </div>
    )
}
export default applianceTable