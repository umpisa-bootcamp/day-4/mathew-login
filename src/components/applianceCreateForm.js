import React, {useState, useEffect} from 'react';

const ApplianceCreateForm = ({
    onCreate,
    appliance,
    onEdit,
    onCancel,
}) => {
    const [state, setState] = useState({
        name: appliance ? appliance.name : '',
        specification: appliance ? appliance.specification:'',
    });

    useEffect(() => {
        setState({
            ...state,
            name: appliance.name,
            specification: appliance.specification
        })
    },[appliance])

    const onChangeField = (event, field) => {
        setState({
            ...state,
            [field]: event.target.value
        })
        // console.warn("field", field, event.target.value, state)
    }

    const onSubmit = (data) => {
        if (Object.keys(appliance) > 0){
            onEdit(data, appliance.id);
        } else{
            onCreate(data)
        }
    }

    return (
        <div>
            <p>Name</p>
            <input 
                type="text" 
                onChange={e => onChangeField(e, 'name')} 
                defaultValue={state.name} />            
            <p>Specification</p>
            <input 
                type="text" 
                onChange={e => onChangeField(e, 'specification')} 
                defaultValue={state.specification} />    
                <br />
                <p>
                <button onClick={() => onCancel()}>CANCEL</button>
                <button onClick={() => onSubmit(state)}>CREATE</button>

                </p>        
        </div>
    )
}
export default ApplianceCreateForm;