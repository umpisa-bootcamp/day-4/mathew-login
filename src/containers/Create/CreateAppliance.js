import React, {useState, useEffect} from 'react';
import ApplianceCreateForm from '../../components/applianceCreateForm';
import {
  useHistory
} from 'react-router-dom';

function CreateAppliance() {
  const history = useHistory()
  const historyState = history.location.state
  const [state, setState] = useState({
    appliance: historyState ? historyState.appliance : {},
  })

  const insertAppliance = (data) => {
    fetch('http://localhost:3001/v1/appliances' , {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(data)
    }).then(()=> history.push('/home'))
  }
  const editAppliance = (data, id) => {
    fetch(`http://localhost:3001/v1/appliances/${id}` , {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        name: data.name,
        specification: data.specification
      })
    }).then(()=> history.push('/home'))
  }
  const onCancel = () => {
    history.push('/')
  }
  return (
    <div style={{textAlign: 'center'}}>
        <ApplianceCreateForm
        appliance={state.appliance}
        onSubmit={insertAppliance}
        onEdit={editAppliance}
        onCancel={onCancel}/>
    </div>
  );
}

export default CreateAppliance;
