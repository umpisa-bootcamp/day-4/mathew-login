import React, {useState, useEffect} from 'react';
import UserLogin from '../../components/userLogin';
import {
    useHistory
  } from 'react-router-dom';

function Login(){
    const history = useHistory()
    const historyState = history.location.state
    const [state, setState] = useState({
        user: historyState ? historyState.user : {},
    })

    const loginUser = (data) => {
        fetch('http://localhost:3001/v1/auth/login', {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
        }).then(() => history.push('/home'))
    }
    return(
        <div style={{textAlign: 'center'}}>
        <UserLogin
        user={state.user}
        onSubmit={loginUser} />
    </div>
    )
}
export default Login
