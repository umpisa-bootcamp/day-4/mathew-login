import React, {useState, useEffect} from 'react';
import UserRegister from '../../components/userRegister';
import {
    useHistory
  } from 'react-router-dom';

function Register(){
    const history = useHistory()
    const historyState = history.location.state
    const [state, setState] = useState({
        user: historyState ? historyState.user : {},
    })

    const registerUser = (data) => {
        fetch('http://localhost:3001/v1/auth/register', {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
        }).then(() => history.push('/home'))
    }
    return(
        <div style={{textAlign: 'center'}}>
        <UserRegister
        user={state.user}
        onSubmit={registerUser} />
    </div>
    )
}
export default Register