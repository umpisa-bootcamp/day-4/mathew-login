import React, {useState, useEffect} from 'react';
import {
  useHistory
} from 'react-router-dom'
import ApplianceTable from '../../components/applianceTable';

function HomePage() {
  const history = useHistory()
  const [state, setState] = useState({
    name: 'Jules',
    index: true,
    appliances: [],
    appliance: {},
  })

  const fetchAppliances = () => {
    fetch('http://localhost:3001/v1/appliances')
      .then(res => res.json())
      .then(res => setState({...state, appliances: res.data}))
  }
  const deleteAppliance = (id) => {
    fetch(`http://localhost:3001/v1/appliances/${id}` , {
      method: 'DELETE',
    }).then(()=> fetchAppliances())
  }

  useEffect(() => {
    fetchAppliances();
  },[])

  const onClickRow = (appliance) => {
    setState({
      ...state,
      appliance
    })
  }

  const onClickEdit = () =>{
    history.push({
      pathname: '/create',
      state: {
        appliance: {}
      }
    })
  }
  const onClickCreate = () => {
    history.push({
      pathname: '/create',
      state: {
        appliance: {}
      }
    })
  }

  return (
    <div className="App">
     <header className="App-header">
       <button onClick={onClickCreate}>CREATE</button>
        <ApplianceTable onClickRow={onClickRow}
         fetchAppliances={fetchAppliances}
         deleteAppliance={deleteAppliance}
         appliances={state.appliances}
         onClickEdit={onClickEdit}/>
      </header>
    </div>
  );
}

export default HomePage;
