import React, {useState, useEffect} from "react";

const NewComponent = ({
    name,
    onButtonClick
}) => {
    const [state, setState] = useState({
        count: 0,
    });

    const onButtonClickCount = () => {
        setState({count: state.count + 1});
    }

    useEffect(() => {
        console.warn("newWarn", name)
    }, [name])

    useEffect(() => {
        console.warn("newState", state)
    }, [state])
    return(
        <div>
        <button onClick={onButtonClick}>Change Name</button>
        <h1>{name}</h1>
        <button onClick={onButtonClickCount}>Change Name</button>
        <h1>{state.count}</h1>
        </div>
    )
}

export default NewComponent